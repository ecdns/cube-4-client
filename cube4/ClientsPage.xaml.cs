﻿using cube4.Models;
using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;

namespace cube4
{
    public partial class ClientsPage : Window
    {
        public ObservableCollection<Client> Clients { get; set; }

        public ClientsPage()
        {
            InitializeComponent();

            // Charger les clients depuis l'API au moment de l'initialisation de la fenêtre
            LoadClientsAsync();
        }

        public async Task LoadClientsAsync()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = await client.GetAsync("http://localhost:5273/api/clients");
                    response.EnsureSuccessStatusCode(); // Lève une exception en cas d'erreur HTTP

                    // Lire le contenu de la réponse en tant que chaîne JSON
                    string json = await response.Content.ReadAsStringAsync();

                    // Désérialiser la chaîne JSON en une liste de clients
                    Clients = Newtonsoft.Json.JsonConvert.DeserializeObject<ObservableCollection<Client>>(json);

                    // Définition du DataContext une fois les données chargées
                    DataContext = this;
                }
            }
            catch (HttpRequestException ex)
            {
                // Gérer les erreurs liées à la requête HTTP
                MessageBox.Show($"Erreur HTTP : {ex.Message}");
            }
            catch (Exception ex)
            {
                // Gérer toute autre exception
                MessageBox.Show($"Une erreur s'est produite : {ex.Message}");
            }
        }


        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            // Redirection vers la page d'accueil
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            Close();
        }

        private void AjouterClient_Click(object sender, RoutedEventArgs e)
        {
            // Ouvrir une nouvelle fenêtre pour le formulaire de création de client
            CreerClientWindow creerClientWindow = new CreerClientWindow();
            creerClientWindow.ShowDialog();
        }

        private void ModifierClient_Click(object sender, RoutedEventArgs e)
        {
            var clientSelectionne = (Client)((FrameworkElement)sender).DataContext;
            ModifierClientWindow modifierClient = new ModifierClientWindow(clientSelectionne);
            modifierClient.Show();
        }

        private void SupprimerClient_Click(object sender, RoutedEventArgs e)
        {
            // Code pour supprimer le client sélectionné
        }
    }
}
