﻿public class ProduitCommande
{
    public int ProduitId { get; set; }
    public int Quantite { get; set; }

    // Constructeur par défaut
    public ProduitCommande()
    {
    }

    // Constructeur avec paramètres
    public ProduitCommande(int produitId, int quantite)
    {
        ProduitId = produitId;
        Quantite = quantite;
    }
}
