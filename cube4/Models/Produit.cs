﻿using System;

namespace cube4.Models
{
    public class Produit
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public string Appellation { get; set; }
        public string Cepage { get; set; }
        public string Region { get; set; }
        public int Annee { get; set; }
        public double DegreAlcool { get; set; }
        public decimal PrixAchat { get; set; }
        public decimal PrixVente { get; set; }
        public DateTime DatePeremption { get; set; }
        public bool EnPromotion { get; set; }
        public int FamilleProduitId { get; set; }
        public int FournisseurId { get; set; }
    }
}
