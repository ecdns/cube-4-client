﻿public class Adresse
{
    public string Rue { get; set; }
    public string CodePostal { get; set; }
    public string Ville { get; set; }
    public string Pays { get; set; }
}

public class Fournisseur
{
    public int Id { get; set; }
    public string Nom { get; set; }
    public Adresse Adresse { get; set; }
    public string Telephone { get; set; }
    public string Email { get; set; }
}
