﻿using System.Collections.ObjectModel;
using System;
using System.Collections.Generic;

namespace cube4.Models
{
    public class CommandeFournisseur
    {
        public int Id { get; set; }
        public DateTime DateCommande { get; set; }
        public DateTime DateReception { get; set; }
        public string Statut { get; set; }
        public Fournisseur Fournisseur { get; set; }
        public Employe Employe { get; set; }
        public List<LigneCommandeFournisseur> LigneCommandeFournisseurs { get; set; }
    }

    public class Fournisseur
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public Adresse Adresse { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
    }

    public class Adresse
    {
        public string Rue { get; set; }
        public string CodePostal { get; set; }
        public string Ville { get; set; }
        public string Pays { get; set; }
    }

    public class Employe
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Email { get; set; }
        public DateTime? DateEmbauche { get; set; }
        public DateTime? DateDepart { get; set; }
        public string Poste { get; set; }
    }

    public class LigneCommandeFournisseur
    {
        public int Id { get; set; }
        public int Quantite { get; set; }
        public decimal PrixUnitaire { get; set; }
        public decimal Remise { get; set; }
        public decimal Total { get; set; }
        public Produit Produit { get; set; }
        public int CommandeFournisseurId { get; set; }
    }

    public class Promotion
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime DateFin { get; set; }
        public decimal Pourcentage { get; set; }
    }

}

public class CommandeClient
    {
        public string Client { get; set; }
        public string Produit { get; set; }
        public int Quantite { get; set; }
    }