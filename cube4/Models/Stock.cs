﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.Generic;

namespace cube4.Models
{
    public class Stock : INotifyPropertyChanged
    {
        private int _id;
        public int id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        private int _quantite;
        public int quantite
        {
            get { return _quantite; }
            set { SetProperty(ref _quantite, value); }
        }

        private int _seuilDisponibilite;
        public int seuilDisponibilite
        {
            get { return _seuilDisponibilite; }
            set { SetProperty(ref _seuilDisponibilite, value); }
        }

        private string _statut;
        public string statut
        {
            get { return _statut; }
            set { SetProperty(ref _statut, value); }
        }

        private Produit _produit;
        public Produit produit
        {
            get { return _produit; }
            set { SetProperty(ref _produit, value); }
        }

        private DateTime? _dateCreation;
        public DateTime? dateCreation
        {
            get { return _dateCreation; }
            set { SetProperty(ref _dateCreation, value); }
        }

        private DateTime? _dateModification;
        public DateTime? dateModification
        {
            get { return _dateModification; }
            set { SetProperty(ref _dateModification, value); }
        }

        private DateTime? _dateSuppression;
        public DateTime? dateSuppression
        {
            get { return _dateSuppression; }
            set { SetProperty(ref _dateSuppression, value); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetProperty<T>(ref T backingField, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(backingField, value))
                return false;

            backingField = value;
            OnPropertyChanged(propertyName);
            return true;
        }
    }
}
