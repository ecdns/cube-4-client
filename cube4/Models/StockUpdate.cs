﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cube4.Models
{
    internal class StockUpdate
    {
        public int id { get; }
        public int quantite { get; set; }
        public int seuilDisponibilite { get; set; }
    }
}
