﻿using cube4.Models;
using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;

namespace cube4
{
    public partial class ProductPage : Window
    {
        public ObservableCollection<Produit> Produits { get; set; } = new ObservableCollection<Produit>();

        public ProductPage()
        {
            InitializeComponent();
            LoadProduitsAsync(); // Appel de la méthode asynchrone
        }

        public async Task LoadProduitsAsync()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = await client.GetAsync("http://localhost:5273/api/produits");
                    response.EnsureSuccessStatusCode(); // Lève une exception en cas d'erreur HTTP

                    string json = await response.Content.ReadAsStringAsync();
                    Produits = Newtonsoft.Json.JsonConvert.DeserializeObject<ObservableCollection<Produit>>(json);
                    DataContext = this; // Définition du DataContext une fois les données chargées
                }
            }
            catch (HttpRequestException ex)
            {
                // Gérer les erreurs liées à la requête HTTP
                MessageBox.Show($"Erreur HTTP : {ex.Message}");
            }
            catch (Exception ex)
            {
                // Gérer toute autre exception
                MessageBox.Show($"Une erreur s'est produite : {ex.Message}");
            }
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            // Redirection vers la page d'accueil
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            Close();
        }

        private void AjouterProduit(object sender, RoutedEventArgs e)
        {
            CreerProduitWindow creerProduitWindow = new CreerProduitWindow();
            creerProduitWindow.Show();
        }


        private void ModifierProduit_Click(object sender, RoutedEventArgs e)
        {
            // Récupérer le produit sélectionné
            var produitSelectionne = (Produit)((FrameworkElement)sender).DataContext;

            // Passer le produit à la fenêtre ModifierProduitWindow
            ModifierProduitWindow modifierProduitWindow = new ModifierProduitWindow(produitSelectionne, this);
            modifierProduitWindow.Show();
        }

        private async void SupprimerProduit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var produitSelectionne = (Produit)((FrameworkElement)sender).DataContext;
                // Récupérer l'identifiant du produit à supprimer

                // Créer un client HTTP
                using (HttpClient client = new HttpClient())
                {
                    // Envoyer une requête DELETE à l'URL correspondant à l'identifiant du produit
                    HttpResponseMessage response = await client.DeleteAsync($"http://localhost:5273/api/produits/{produitSelectionne.Id}");

                    // Vérifier si la requête a réussi
                    if (response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Produit supprimé avec succès.");
                    }
                    else
                    {
                        MessageBox.Show("Échec de la suppression du produit.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur s'est produite : " + ex.Message);
            }
        }
}
}
