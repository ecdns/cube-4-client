﻿using cube4.Models;
using System.Windows;

namespace cube4
{
    public partial class CreerClientWindow : Window
    {
        public CreerClientWindow()
        {
            InitializeComponent();
        }

        private void CreerClient_Click(object sender, RoutedEventArgs e)
        {

        // Créer un nouvel objet Client avec les valeurs récupérées des champs du formulaire
        Client nouveauClient = new Client
            {
                Nom = txtNom.Text,
                Prenom = txtPrenom.Text,
                Adresse = txtAdresse.Text,
                Telephone = txtTelephone.Text,
                Username = txtUsername.Text,
                CodePostal = txtCodePostal.Text,
                Ville = txtVille.Text,
                Pays = txtPays.Text,
                Email = txtEmail.Text
            };

            this.Close();
        }
    }
}
