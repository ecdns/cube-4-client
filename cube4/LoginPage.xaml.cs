﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace cube4
{
    public partial class LoginPage : Window
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        private async void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            // Récupérer l'email et le mot de passe saisis par l'utilisateur
            string email = txtUsername.Text;
            string password = txtPassword.Password;

            try
            {
                // Créer un objet JSON contenant les informations d'identification
                string jsonCredentials = Newtonsoft.Json.JsonConvert.SerializeObject(new
                {
                    email = email,
                    password = password
                });

                // Créer un client HTTP
                using (HttpClient client = new HttpClient())
                {
                    // Envoyer une requête POST à l'URL de connexion avec les informations d'identification
                    HttpResponseMessage response = await client.PostAsync("http://localhost:5273/api/auth/login",
                        new StringContent(jsonCredentials, Encoding.UTF8, "application/json"));
                    // Vérifier si la requête a réussi
                    if (response.IsSuccessStatusCode)
                    {
                        // Récupérer le jeton d'authentification de la réponse
                        string token = await response.Content.ReadAsStringAsync();

                        // Stocker le jeton dans une propriété statique ou dans un endroit sécurisé de votre application
                        // Par exemple, vous pouvez le stocker dans une classe d'authentification ou dans les paramètres de votre application
                        // MainWindow.AuthenticationToken = token;

                        // Afficher un message de connexion réussie
                        MessageBox.Show("Login successful! Redirecting...");

                        // Fermer la fenêtre de connexion
                        this.Close();

                        // Vous pouvez rediriger l'utilisateur vers une autre page ou effectuer d'autres actions nécessaires après la connexion réussie
                    }
                    else
                    {
                        // Afficher un message d'erreur si les informations d'identification sont incorrectes
                        MessageBox.Show("Invalid email or password. Please try again.");
                    }
                }
            }
            catch (Exception ex)
            {
                // Afficher un message en cas d'erreur
                MessageBox.Show("An error occurred: " + ex.Message);
            }
        }

        private void CloseWindow_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
