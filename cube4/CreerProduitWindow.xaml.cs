﻿using cube4.Models;
using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Windows;

namespace cube4
{
    public partial class CreerProduitWindow : Window
    {
        public CreerProduitWindow()
        {
            InitializeComponent();
        }

        private async void CreerProduit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Produit nouveauProduit = new Produit
                {
                    Nom = txtNom.Text,
                    Description = txtDescription.Text,
                    Appellation = txtAppellation.Text,
                    Cepage = txtCepage.Text,
                    Region = txtRegion.Text,
                    Annee = Convert.ToInt32(txtAnnee.Text),
                    DegreAlcool = Convert.ToDouble(txtDegreAlcool.Text),
                    PrixAchat = Convert.ToDecimal(txtPrixAchat.Text),
                    PrixVente = Convert.ToDecimal(txtPrixVente.Text),
                    DatePeremption = datePickerPeremption.SelectedDate ?? DateTime.Now,
                    FamilleProduitId = Convert.ToInt32(txtFamilleProduitId.Text),
                    FournisseurId = Convert.ToInt32(txtFournisseurId.Text)
                };

                // Envoyer le produit à l'API pour création
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = await client.PostAsJsonAsync("http://localhost:5273/api/produits", nouveauProduit);
                    response.EnsureSuccessStatusCode(); // Lève une exception en cas d'erreur HTTP

                    MessageBox.Show("Produit créé avec succès.");
                }
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Une erreur s'est produite : {ex.Message}");
            }
        }
    }
}
