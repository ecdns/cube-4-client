﻿using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using cube4.Models;
using System.Reflection;

namespace cube4
{
    public partial class InventoryPage : Window
    {
        public ObservableCollection<Stock> Stocks { get; set; } = new ObservableCollection<Stock>();

        public InventoryPage()
        {
            InitializeComponent();
            LoadStocksAsync();
        }

        private async Task LoadStocksAsync()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = await client.GetAsync("http://localhost:5273/api/stocks");
                    response.EnsureSuccessStatusCode();

                    string json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    ObservableCollection<Stock> tempStocks = JsonConvert.DeserializeObject<ObservableCollection<Stock>>(json);

                    // Utiliser Dispatcher.Invoke pour mettre à jour la collection Stocks sur le thread principal
                    Dispatcher.Invoke(() =>
                    {
                        Stocks.Clear();
                        foreach (var stock in tempStocks)
                        {
                            if (stock.quantite == 0)
                            {
                                AlertOutOfStock alertWindow = new AlertOutOfStock(stock);
                                alertWindow.ShowDialog();
                            }
                            Stocks.Add(stock);
                        }
                        DataContext = this;
                    });
                }
            }
            catch (HttpRequestException ex)
            {
                MessageBox.Show($"HTTP Error: {ex.Message}");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred: {ex.Message}");
            }
        }

        private async void StocksDataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            try
            {
                var cellContent = e.EditingElement as TextBox; // Supposons que la cellule est une TextBox, ajustez si nécessaire
                if (cellContent != null)
                {
                    var modifiedValue = cellContent.Text; // Obtenez la valeur modifiée de la cellule
                    var modifiedStock = (Stock)e.Row.Item;

                    var stockUpdate = new StockUpdate();

                    // Mettez à jour la propriété de quantité avec la nouvelle valeur modifiée
                    stockUpdate.quantite = int.Parse(modifiedValue);
                    stockUpdate.seuilDisponibilite = stockUpdate.seuilDisponibilite;

                    using (HttpClient client = new HttpClient())
                    {
                        string jsonStock = JsonConvert.SerializeObject(stockUpdate);
                        StringContent content = new StringContent(jsonStock, Encoding.UTF8, "application/json");

                        HttpResponseMessage response = await client.PutAsync("http://localhost:5273/api/produits/" + modifiedStock.id, content);

                        // Afficher la réponse complète dans la console
                        string responseContent = await response.Content.ReadAsStringAsync();
                        Console.WriteLine(responseContent); // Affiche le contenu de la réponse dans la console
                    }
                }
            }
            catch (HttpRequestException ex)
            {
                MessageBox.Show($"HTTP Error: {ex.Message}");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred: {ex.Message}");
            }
        }




        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            Close();
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
