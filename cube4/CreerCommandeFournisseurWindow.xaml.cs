﻿using cube4.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace cube4
{
    public partial class CreerCommandeFournisseurWindow : Window
    {
        public Stock StockItem { get; set; }
        private readonly HttpClient _httpClient = new HttpClient();

        public CreerCommandeFournisseurWindow(Stock stockItem)
        {
            InitializeComponent();
            StockItem = stockItem;
            if (StockItem != null)
            {
                RemplirChampFournisseurId(StockItem.produit.Id);
                txtProduitId.Text = StockItem.produit.Id.ToString();
            }
        }

        private async Task RemplirChampFournisseurId(int idProduit)
        {
            string url = $"http://localhost:5273/api/fournisseurs/produit/{idProduit}";

            try
            {
                HttpResponseMessage response = await _httpClient.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    Fournisseur fournisseur = JsonConvert.DeserializeObject<Fournisseur>(json);
                    if (fournisseur != null)
                    {
                        txtFournisseurId.Text = fournisseur.Id.ToString();
                    }
                }
                else
                {
                    MessageBox.Show($"Erreur HTTP {response.StatusCode} lors de la requête à {url}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Une erreur s'est produite : {ex.Message}");
            }
        }

        private async void CreerCommandeFournisseur_Click(object sender, RoutedEventArgs e)
        {
            object commandeFournisseur = null;

            if (StockItem != null)
            {
                commandeFournisseur = new
                {
                    fournisseurId = txtFournisseurId.Text,
                    employeId = 1,
                    produits = new[]
                    {
                        new
                        {
                            produitId = StockItem.produit.Id,
                            quantite = int.Parse(txtQuantite.Text)
                        }
                    }
                };
            }
            else
            {
                commandeFournisseur = new
                {
                    fournisseurId = int.Parse(txtFournisseurId.Text),
                    employeId = 1,
                    produits = new[]
                    {
                        new
                        {
                            produitId = int.Parse(txtProduitId.Text),
                            quantite = int.Parse(txtQuantite.Text)
                        }
                    }
                };
            }

            var json = JsonConvert.SerializeObject(commandeFournisseur);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync("http://localhost:5273/api/commandes-fournisseurs", content);
            var responseContent = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                MessageBox.Show("Commande créée avec succès!");
                Close();
            }
            else
            {
                MessageBox.Show("Erreur lors de la création de la commande. Veuillez réessayer.");
            }
        }
    }
}
