﻿using cube4.Models;
using System;
using System.Windows;

namespace cube4
{
    public partial class ModifierClientWindow : Window
    {
        // Propriété pour stocker le client à modifier
        private readonly Client client;
        public ModifierClientWindow(Client client)
        {
            InitializeComponent();
            this.client = client; // Stockage du client

            // Pré-remplissage des champs avec les valeurs du client
            txtUsername.Text = client.Username;
            txtNom.Text = client.Nom;
            txtPrenom.Text = client.Prenom;
            txtAdresse.Text = client.Adresse;
            txtCodePostal.Text = client.CodePostal;
            txtVille.Text = client.Ville;
            txtPays.Text = client.Pays;
            txtTelephone.Text = client.Telephone;
            txtEmail.Text = client.Email;
        }

        private void Enregistrer_Click(object sender, RoutedEventArgs e)
        {
            // Récupération des valeurs saisies dans les champs
            string username = txtUsername.Text;
            string nom = txtNom.Text;
            string prenom = txtPrenom.Text;
            string adresse = txtAdresse.Text;
            string codePostal = txtCodePostal.Text;
            string ville = txtVille.Text;
            string pays = txtPays.Text;
            string telephone = txtTelephone.Text;
            string email = txtEmail.Text;

            client.Username = username;
            client.Nom = nom;
            client.Prenom = prenom;
            client.Adresse = adresse;
            client.CodePostal = codePostal;
            client.Ville = ville;
            client.Pays = pays;
            client.Telephone = telephone;
            client.Email = email;

            // Ici, vous pouvez effectuer des opérations de mise à jour ou d'enregistrement du client dans votre système

            // Fermez la fenêtre après l'enregistrement
            this.Close();
        }
    }
}
