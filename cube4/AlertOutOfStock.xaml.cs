﻿using cube4.Models;
using System;
using System.Windows;

namespace cube4
{
    public partial class AlertOutOfStock : Window
    {
        // Propriété pour stocker l'objet Stock
        public Stock StockItem { get; set; }

        // Constructeur prenant un objet Stock en paramètre
        public AlertOutOfStock(Stock stock)
        {
            InitializeComponent();

            // Stocker l'objet Stock dans la propriété
            StockItem = stock;
            DataContext = StockItem;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void CreateOrderButton_Click(object sender, RoutedEventArgs e)
        {
            CreerCommandeFournisseurWindow creerCommandeFournisseurWindow = new CreerCommandeFournisseurWindow(StockItem);
            Close();
            creerCommandeFournisseurWindow.Show();
        }
    }
}
