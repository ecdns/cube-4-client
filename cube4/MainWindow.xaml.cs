﻿using System.Windows;

namespace cube4
{
    public partial class MainWindow : Window
    {
        public static bool IsUserLoggedIn { get; set; } = false;

        public MainWindow()
        {
            InitializeComponent();

            // Vérifiez si l'utilisateur est connecté
            if (!IsUserLoggedIn)
            {
                // Si l'utilisateur n'est pas connecté, afficher la page de connexion
                LoginPage loginPage = new LoginPage();
                loginPage.ShowDialog();
                IsUserLoggedIn = true;
            }
        }

        private void Customer_Click(object sender, RoutedEventArgs e)
        {
            // Redirection vers la page des clients
            ClientsPage clientPage = new ClientsPage();
            clientPage.Show();
            Close();
        }

        private void Inventory_Click(object sender, RoutedEventArgs e)
        {
            InventoryPage inventoryPage = new InventoryPage();
            inventoryPage.Show();
            Close();
        }

        private void Orders_Click(object sender, RoutedEventArgs e)
        {
            // Redirection vers la page des clients
            OrdersPage ordersPage = new OrdersPage();
            ordersPage.Show();
            Close();
        }

        private void Products_Click(object sender, RoutedEventArgs e)
        {
            // Redirection vers la page des clients
            ProductPage productPage = new ProductPage();
            productPage.Show();
            Close();
        }

        private void CloseWindow_Click(object sender, RoutedEventArgs e)
        {
            // Fermer la fenêtre
            Close();
        }
    }
}
