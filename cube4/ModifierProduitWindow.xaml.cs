﻿using cube4.Models;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace cube4
{
    public partial class ModifierProduitWindow : Window
    {
        // Propriété pour stocker le produit à modifier
        private readonly Produit produit;
        private readonly ProductPage productPage; // Référence à la page ProductPage

        public ModifierProduitWindow(Produit produit, ProductPage productPage)
        {
            InitializeComponent();
            this.produit = produit; // Stockage du produit
            this.productPage = productPage; // Stockage de la référence à ProductPage

            // Pré-remplissage des champs avec les valeurs du produit
            txtNom.Text = produit.Nom;
            txtDescription.Text = produit.Description;
            txtAppellation.Text = produit.Appellation;
            txtCepage.Text = produit.Cepage;
            txtRegion.Text = produit.Region;
            txtAnnee.Text = produit.Annee.ToString();
            txtDegreAlcool.Text = produit.DegreAlcool.ToString();
            txtPrixAchat.Text = produit.PrixAchat.ToString();
            txtPrixVente.Text = produit.PrixVente.ToString();
            dpDatePeremption.SelectedDate = produit.DatePeremption;
            txtFamilleProduitId.Text = produit.FamilleProduitId.ToString();
            txtFournisseurId.Text = produit.FournisseurId.ToString();
        }

        private async void Enregistrer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Récupération des valeurs saisies dans les champs
                string nom = txtNom.Text;
                string description = txtDescription.Text;
                string appellation = txtAppellation.Text;
                string cepage = txtCepage.Text;
                string region = txtRegion.Text;
                int annee = int.Parse(txtAnnee.Text);
                double degreAlcool = double.Parse(txtDegreAlcool.Text);
                decimal prixAchat = decimal.Parse(txtPrixAchat.Text);
                decimal prixVente = decimal.Parse(txtPrixVente.Text);
                DateTime datePeremption = dpDatePeremption.SelectedDate ?? DateTime.MinValue;
                int familleProduitId = int.Parse(txtFamilleProduitId.Text);
                int fournisseurId = int.Parse(txtFournisseurId.Text);

                // Création d'un objet Produit avec les nouvelles valeurs
                Produit nouveauProduit = new Produit
                {
                    Id = produit.Id,
                    Nom = nom,
                    Description = description,
                    Appellation = appellation,
                    Cepage = cepage,
                    Region = region,
                    Annee = annee,
                    DegreAlcool = degreAlcool,
                    PrixAchat = prixAchat,
                    PrixVente = prixVente,
                    DatePeremption = datePeremption,
                    FamilleProduitId = familleProduitId,
                    FournisseurId = fournisseurId
                };

                // Conversion de l'objet Produit en JSON
                string jsonProduit = Newtonsoft.Json.JsonConvert.SerializeObject(nouveauProduit);

                // Création du client HTTP
                using (HttpClient client = new HttpClient())
                {
                    // Création de la requête PUT
                    HttpResponseMessage response = await client.PutAsync("http://localhost:5273/api/produits/" + produit.Id,
                                                                           new StringContent(jsonProduit, Encoding.UTF8, "application/json"));

                    // Afficher la réponse complète dans la console
                    string responseContent = await response.Content.ReadAsStringAsync();

                    // Vérification de la réussite de la requête
                    if (response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Produit mis à jour avec succès.");

                        // Appeler LoadProduitsAsync de ProductPage après la mise à jour du produit
                        await productPage.LoadProduitsAsync();

                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Échec de la mise à jour du produit.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur s'est produite : " + ex.Message);
            }
            this.Close();
        }
    }
}
