﻿using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using cube4.Models;
using Newtonsoft.Json;

namespace cube4
{
    public partial class OrdersPage : Window
    {
        public ObservableCollection<CommandeFournisseur> CommandesFournisseurs { get; set; }
        public ObservableCollection<CommandeClient> CommandesClients { get; set; }
        private readonly HttpClient _httpClient = new HttpClient();

        public OrdersPage()
        {
            InitializeComponent();

            // Chargement des commandes fournisseurs
            LoadCommandesFournisseurs();

            // Chargement des commandes clients
            /*LoadCommandesClients();*/
        }

        public async Task LoadCommandesFournisseurs()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = await client.GetAsync("http://localhost:5273/api/commandes-fournisseurs");
                    response.EnsureSuccessStatusCode(); // Lève une exception en cas d'erreur HTTP

                    string json = await response.Content.ReadAsStringAsync();
                    CommandesFournisseurs = JsonConvert.DeserializeObject<ObservableCollection<CommandeFournisseur>>(json);
                    Console.WriteLine(CommandesFournisseurs[0].Employe);
                    DataContext = this; // Définition du DataContext une fois les données chargées
                }
            }
            catch (HttpRequestException ex)
            {
                // Gérer les erreurs liées à la requête HTTP
                MessageBox.Show($"Erreur HTTP : {ex.Message}");
            }
            catch (Exception ex)
            {
                // Gérer toute autre exception
                MessageBox.Show($"Une erreur s'est produite : {ex.Message}");
            }
        }

        public async Task LoadCommandesClients()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = await client.GetAsync("http://localhost:5273/api/commandes-clients/commande");
                    response.EnsureSuccessStatusCode();

                    string json = await response.Content.ReadAsStringAsync();
                    CommandesClients = JsonConvert.DeserializeObject<ObservableCollection<CommandeClient>>(json);
                    DataContext = this;
                }
            }
            catch (HttpRequestException ex)
            {
                // Gérer les erreurs liées à la requête HTTP
                MessageBox.Show($"Erreur HTTP : {ex.Message}");
            }
            catch (Exception ex)
            {
                // Gérer toute autre exception
                MessageBox.Show($"Une erreur s'est produite : {ex.Message}");
            }
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            // Redirection vers la page d'accueil
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            Close();
        }

        private void CreerCommandeFournisseur_Click(object sender, RoutedEventArgs e)
        {
            Stock stockItem = null; // initialisation de stockItem
            CreerCommandeFournisseurWindow commandeFournisseur = new CreerCommandeFournisseurWindow(stockItem);
            commandeFournisseur.Show();
        }
    }
}
